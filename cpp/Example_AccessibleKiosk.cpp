//This example demonstrates how the accessible kiosk can output directions

#include <cstdlib>
#include <iostream>
#include <string>
#include <chrono>
#include <thread>

#include <UltrahapticsAmplitudeModulation.hpp>


int main(int argc, char* argv[])
{
    // Create an emitter.
    Ultrahaptics::AmplitudeModulation::Emitter emitter;

    // Set frequency to 200 Hertz and maximum intensity
    float frequency = 200.0 * Ultrahaptics::Units::hertz;
    float intensity = 1.0f;

    // Position the focal point at 20 centimeters above the array.
    float distance = 20.0 * Ultrahaptics::Units::centimetres;

    //side resolution for interpolation
    float sideResolution = 100;

    //side length for lines
    float side_length = 100 * Ultrahaptics::Units::mm;



    // Optionally, specify the focal point distance in cm on the command line
        if (argc > 1)
        {
            distance = atof(argv[1]) * Ultrahaptics::Units::centimetres;
        }


    Ultrahaptics::Vector3 position1(0.0f, 0.0f, distance);
    Ultrahaptics::AmplitudeModulation::ControlPoint point1(position1, intensity, frequency);

    Ultrahaptics::Vector3 start_position = position1;

    //make the point move upwards from centre for 60mm
    while(position1.y - start_position.y <= 60 * Ultrahaptics::Units::mm)
    {
        position1.y += side_length / sideResolution;
        point1.setPosition(position1);


        emitter.update(point1);
        //set delay
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    //make the point pulsate to indicate change of direction


    bool switchDirection = true;
    std::cout << "Changing direction now!" << std::endl;

    for (int i = 0; i < 6; i++)
    {
        if (switchDirection)
        {
            position1.z += 1000 * Ultrahaptics::Units::mm;
        }
        else
        {
            position1.z -= 1000 * Ultrahaptics::Units::mm;
        }

        point1.setPosition(position1);
        emitter.update(point1);
        switchDirection = !switchDirection;

        //set delay
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
    std::cout << "End changing direction" << std::endl;

    start_position = position1;
    while(position1.x - start_position.x <= 60 * Ultrahaptics::Units::mm)
    {
        position1.x += side_length / sideResolution;
        point1.setPosition(position1);


        emitter.update(point1);
        //set delay
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }


    // Emit the point.
    if (!emitter.update(point1))
    {
        std::cout << "Couldn't start emitter." << std::endl;
        return 1;
    }

    // Emitter shuts down on exit.
    return 0;

}
